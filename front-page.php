<?php get_header();?>
<div data-bs-spy="scroll" data-bs-target="#navbarSupportedContent" data-bs-root-margin="0px 0px -40%"
    data-bs-smooth-scroll="true">
    <div class="overflow-hidden">
      <section id="inicio">
        <div class="swiper mySwiper">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="img-swipper">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/inicio/header1.jpg" />
              </div>
            </div>
            <div class="swiper-slide">
              <div class="img-swipper">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/inicio/header2.jpg?v=2" />
              </div>
            </div>
            <div class="swiper-slide">
              <div class="img-swipper">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/inicio/header3.jpg" />
              </div>
            </div>
            <div class="swiper-slide">
              <div class="img-swipper">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/inicio/header4.jpg" />
              </div>
            </div>
            <div class="swiper-slide">
              <div class="img-swipper">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/inicio/header5.jpg" />
              </div>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </section>

      <div class="container">
        <section id="nosotros">
          <div class="row  align-items-center">
            <div class="col col-md-6">
              <h2>Nosotros</h2>
              <div>
                <p>
                  En Centro Auditivo Oiree nos especializamos en problemas auditivos, así como en la realización de
                  estudios para detectar de manera temprana y oportuna problemas auditivos a cualquier edad.
                </p>
                <p> Además, realizamos adaptaciones de auxiliares auditivos de todas las marcas reconocidas ya que somos
                  distribuidores autorizados.
                </p>
                <p> En Centro Auditivo Oiree encontrará solución a cualquier problema relacionado con su audición con el
                  apoyo de la mejor tecnología y de nuestros especialistas que cuentan con más de 15 años de
                  experiencia.
                </p>
                <p> En Centro Auditivo Oiree, tu audición es nuestra pasión.</p>
              </div>
            </div>
            <div class="d-none d-md-block col offset-xl-1 col-5">
              <div class="img-giro">
                <div class="imgs">
                  <img class="img-1" src="<?php echo get_template_directory_uri(); ?>/src/img/nosotros/imagen2.png" alt="">
                  <img class="img-2" src="<?php echo get_template_directory_uri(); ?>/src/img/nosotros/imagen3.png" alt="">
                </div>
                <picture class="img-pulse">
                  <img src="<?php echo get_template_directory_uri(); ?>/src/img/nosotros/imagen1.png">
                  <div class="pulse-1"></div>
                  <div class="pulse-2" style="animation-delay: 0.3s;"></div>
                  <div class="pulse-3" style="animation-delay: 0.6s;"></div>
                </picture>
              </div>
            </div>
          </div>
        </section>
      </div>


      <section id="servicios">
        <h2 class="text-center mb-4">Servicios de audiometría</h2>
        <div class="container">
          <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/audio-metria-tonal.png" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Audiometría Tonal</h5>
                  <p class="card-text">Nos permite mediante una prueba vía aérea y vía ósea conocer el umbral de
                    audición
                    de un paciente.
                  </p>
                </div>
              </a>
            </div>
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/Timpanometria.png" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Timpanometría</h5>
                  <p class="card-text">Este estudio nos permite medir el estado del oído medio la movilidad del tímpano y los huesillos conductores del sonido.
                  </p>
                </div>
              </a>
            </div>
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/tapiz-neonatal.png" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Tamiz Auditivo Neonatal</h5>
                  <p class="card-text">Es un procedimiento sencillo y rápido que permite identificar en los niños si son
                    susceptibles a pérdida de audición.
                  </p>
                </div>
              </a>
            </div>
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/auxiliares-auditivos.jpg" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Venta de Auxiliares Auditivos</h5>
                  <p class="card-text">Contamos con Auxiliares Auditivos Digitales de las más reconocidas marcas a nivel mundial, tenemos la opción indicada para cada persona.</p>
                </div>
              </a>
            </div>
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/tapones.jpg" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Moldes, tapones y protectores auditivos</h5>
                  <p class="card-text">Fabricamos a la medida de sus necesidades, con materiales de la mejor calidad.
                  </p>
                </div>
              </a>
            </div>
            <div class="col">
              <a class="card">
                <img src="<?php echo get_template_directory_uri(); ?>/src/img/servicios/reparacion-aparatos-auditivos.png" class="card-img-top">
                <div class="card-body">
                  <h5 class="card-title">Mantenimiento y reparación</h5>
                  <p class="card-text">Cuidando su economía ofrecemos mantenimiento y reparación de Auxiliares Auditivos en nuestros laboratorios.
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>



    <section id="sucursales" class="container">
      <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4">
        <div class="col d-flex align-items-center">
          <div>
            <h2>Sucursales Oiree</h2>
            <p>Visítanos en cualquiera de nuestras sucursales estamos para brindarte el mejor servicio.</p>
          </div>
        </div>
        <div class="col">
          <div class="card bg-info text-white">
            <div class="card-body">
              <h5 class="card-title">Sucursal Cancún</h5>
              <p class="card-text">
              <ul>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Lun – Vie de 10:00am a 7:00pm y Sab de 10:00am a 2pm
                </li>
                <li>
                  <i class="fa-solid fa-map-pin"></i>
                  Av. Andrés Quintana Roo 161, Smz 45, 77506, Cancún, Q.R.
                </li>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Teléfono: <a target="_blank" href="http://wa.link/0juuj5">998 894 9541</a>
                </li>
                <li>
                  <i class="fa-solid fa-at"></i>
                  <a href="mailto:oi2cancun@gmail.com">oi2cancun@gmail.com</a>
                </li>
              </ul>
              </p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card bg-info text-white">
            <div class="card-body">
              <h5 class="card-title">Sucursal Mérida</h5>
              <p class="card-text">
              <ul>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Lun – Vie de 9:00am a 6:00pm y Sab de 10:00am a 2pm
                </li>
                <li>
                  <i class="fa-solid fa-map-pin"></i>
                  Av. 21 José Díaz Bolio, por 12 y 14 Plaza 21, México Oriente, 97137, Mérida, Yuc.
                </li>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Teléfono: <a target="_blank" href="http://wa.link/0xd3pn">999 992 2779</a>
                </li>
                <li>
                  <i class="fa-solid fa-at"></i>
                  <a href="mailto:oiree.merida@gmail.com">oiree.merida@gmail.com</a>
                </li>
              </ul>
              </p>
            </div>
          </div>
        </div>
        <div class="col">
          <div class="card bg-info text-white">
            <div class="card-body">
              <h5 class="card-title">Sucursal Campeche</h5>
              <p class="card-text">
              <ul>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Lun – Dom de 11:00am a 8:00pm
                </li>
                <li>
                  <i class="fa-solid fa-map-pin"></i>
                  Pedro Sainz de Baranda 139, Área Ah, Galerías Campeche, 24014 Campeche, Camp.

                </li>
                <li>
                  <i class="fa-solid fa-clock"></i>
                  Teléfono: <a target="_blank" href="https://wa.link/c0sakg">991 205 4499</a>
                </li>
                <li>
                  <i class="fa-solid fa-at"></i>
                  <a href="mailto:oireecampeche@gmail.com">oireecampeche@gmail.com</a>
                </li>
              </ul>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="contacto" class="bg-info ">
      <div class="container">
        <div class="row  align-items-end">
          <div class="d-none d-md-block col order-lg-1">
            <div class="img-doctor">
              <img src="<?php echo get_template_directory_uri(); ?>/src/img/contacto/doctor.png" alt="">
            </div>
          </div>
          <div class="col-12 order-lg-0 col-md-6 col-lg-4 offset-lg-2">
            <h2>Contacto</h2>
            <p>¿Quieres agendar una cita? por favor déjanos tus datos de contacto y a la brevedad nos ponemos en
              contacto contigo</p>
              <?php echo apply_shortcodes( '[contact-form-7 id="7" title="Contacto"]' ); ?>
          </div>
        </div>
      </div>
    </section>

  </div>
<?php get_footer();?>