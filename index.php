<?php get_header();?>
<div class="mt-4 pb-4">
    <div class="container">
        <?php while (have_posts()) {the_post();?>
        <h2 class="mb-4">
            <?php the_title() ?>
        </h2>
        <div class="">
            <?php the_content() ?>
        </div>
        <?php }?>
    </div>
</div>
<hr>
<?php get_footer();?>