<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php bloginfo('description'); ?>" />

  <title>
    <?php bloginfo('name'); ?>
    <?php wp_title(); ?>
  </title>
  
  <?php wp_head(); ?>
  <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
</head>

<body class="<?php echo is_admin_bar_showing() ? 'has-admin-bar' : '' ?>">
  <nav id="superior" class="bg-info d-none d-md-block">
    <div class="container">
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <i class="fa-solid fa-clock"></i> Lun - Vie de 10:00am a 7:00pm y Sab de 10:00am a 2:00pm
        </div>
        <div class="row align-items-center">
          <a class="col" href="mailto:oi2cancun@gmail.com">oi2cancun@gmail.com</a>
          <a target="_blank" class="col" href="https://www.facebook.com/CentroAuditivoOiree"><i
              class="fa-brands fa-facebook"></i></a>
          <a class="col" target="_blank" href="http://wa.link/0xd3pn"><i class="fa-brands fa-whatsapp-square"></i></a>
        </div>
      </div>
    </div>
  </nav>
  <nav class="navbar navbar-expand-md navbar-light bg-white sticky-top">
    <div class="container">
      <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/src/img/logo.svg" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item d-none">
            <a class="nav-link" href="<?php echo get_site_url(); ?>#inicio">Nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_site_url(); ?>#nosotros">Nosotros</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_site_url(); ?>#servicios">Servicios</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_site_url(); ?>#sucursales">Sucursales</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo get_site_url(); ?>#contacto">Contacto</a>
          </li>
        </ul>       
      </div>

    </div>
  </nav>