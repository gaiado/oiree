<?php

function oiree_setup()
{

}

function oiree_styles()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/src/css/style.css', array(), '0.0.1');
    wp_enqueue_style('swipper', 'https://unpkg.com/swiper/swiper-bundle.min.css');
    wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css');
    wp_enqueue_style('poppins', '//fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap');
}

function oiree_menus()
{
    register_nav_menus(array(
        'header-menu' => __('Header Menu', 'oiree'),
        'social-menu' => __('Social Menu', 'oiree')
    ));
}

function oiree_promociones()
{

}



//filters
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//Hooks
add_action('after_setup_theme', 'oiree_setup');
add_action('after_setup_theme', 'oiree_menus');
add_action('wp_enqueue_scripts', 'oiree_styles');
add_action('init', 'oiree_promociones');

