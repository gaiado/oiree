<footer>
    <div class="container">
      <div class="row row-cols-1 row-cols-md-2 justify-content-between align-items-center">
        <div class="col text-center text-lg-start">
          © Copyright 2022 Todos los derechos reservados
        </div>
        <div class="col text-center text-lg-end">
          <a href="<?php echo get_site_url(); ?>/aviso-de-privacidad">Aviso de privacidad </a> |
          <a target="_blank" href="https://www.facebook.com/CentroAuditivoOiree">Facebook </a> |
          <a target="_blank" href="http://wa.link/0xd3pn">Whatsapp </a>
        </div>
      </div>
    </div>
  </footer>
  <?php wp_footer()?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
    crossorigin="anonymous"></script>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
  <script>
    var swiper = new Swiper(".mySwiper", {
      effect: "cube",
      grabCursor: true,
      slidesPerView: 1,
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: true,
      },
      cubeEffect: {
        shadow: false,
        slideShadows: false,
        shadowOffset: 20,
        shadowScale: 0.94,
      },
      pagination: {
        el: ".swiper-pagination",
      },
    });
  </script>
</body>

</html>